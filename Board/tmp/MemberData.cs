﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Board.Models;

namespace Board.tmp
{
    public class MemberData
    {
        public List<Member> Members
        {
            get
            {   // 댓글, 작성시간은 미포함
                List<Member> returnValue = new List<Member>
                {
                    new Member { ID = "a",
                        NickName = "a",
                        PW = "a",
                        Group = 1,
                        RegDate = DateTime.Now,
                        isAdmin = false,
                        isBan = false,
                    },
                    new Member { ID = "abd",
                        NickName = "abd",
                        PW = "aaaaaa",
                        Group = 1,
                        RegDate = DateTime.Now,
                        isAdmin = false,
                        isBan = false,
                    },new Member { ID = "abe",
                        NickName = "abe",
                        PW = "aaaaaa",
                        Group = 1,
                        RegDate = DateTime.Now,
                        isAdmin = false,
                        isBan = false,
                    },
                };
                return returnValue;
            }
        }
    }
}