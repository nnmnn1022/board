﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Board.Models;

namespace Board.tmp
{
    public class ArticleData
    {
        public List<Article> Articles
        {
            get
            {   // 댓글, 작성시간은 미포함
                List<Article> returnValue = new List<Article>
                {
                    new Article { Index = 1,
                        Title = "aaa",
                        Contents = "aaaaaa",
                        AuthorID = "aaah"
                    },
                    new Article { Index = 2,
                        Title = "bbb",
                        Contents = "bbbbbb",
                        AuthorID = "bbbh"
                    },
                    new Article { Index = 3,
                        Title = "ccc",
                        Contents = "cccccc",
                        AuthorID = "ccch"
                    },
                    new Article { Index = 4,
                        Title = "ddd",
                        Contents = "dddddd",
                        AuthorID = "dddh"
                    }
                };
                return returnValue;
            }
        }
    }
}