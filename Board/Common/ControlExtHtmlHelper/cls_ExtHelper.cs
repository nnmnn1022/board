﻿/************************************************
**        프로시저 : HtmlHelper 확장 클래스
**        개발일자 : 2014.05.18
**        개발자명 : 
*************************************************/
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Web.Mvc.Html
{
    public static class cls_ExtHelper
    {
        public static MvcHtmlString PageLink(this HtmlHelper source, int curPage, int totalPages, Func<int, string> pageUrl)
        {
            const int pageLimit = 10;

            int startPage = ((curPage - 1) / pageLimit) * pageLimit + 1;
            int endPage = startPage + pageLimit - 1;

            if (totalPages < endPage) endPage = totalPages;

            int nextPage = endPage + 1;

            StringBuilder pageBuilder = new StringBuilder();

            pageBuilder.Append("<ul class='page_view'>");

            TagBuilder first = new TagBuilder("a");
            first.MergeAttribute("href", pageUrl(1));
            first.InnerHtml = "<img src='/images/btn_alpha.gif' alt='첫페이지로' />";
            pageBuilder.AppendLine("<li>" + first.ToString() + "</li>");

            TagBuilder prevTag = new TagBuilder("a");
            prevTag.MergeAttribute("href", pageUrl(startPage == 1 ? 1 : startPage - 1));
            prevTag.InnerHtml = "<img src='/images/btn_prev.gif' alt='이전 10페이지이동' />";
            pageBuilder.AppendLine("<li class='prev_li'>" + prevTag.ToString() + "</li>");

            for (int i = startPage; i <= endPage; i++)
            {
                TagBuilder tag = new TagBuilder("a");
                tag.MergeAttribute("href", pageUrl(i));
                tag.InnerHtml = "" + i;
                if (i == curPage)
                    tag.Attributes.Add("style", "font-weight:bold;color:red");
                pageBuilder.AppendLine(string.Format(@"<li>{0}{1}</li>", tag.ToString(), (i == pageLimit || i == totalPages) ? string.Empty : " | "));
            }

            TagBuilder nextTag = new TagBuilder("a");
            nextTag.MergeAttribute("href", nextPage < totalPages ? pageUrl(nextPage) : pageUrl(totalPages));
            nextTag.InnerHtml = "<img src='/images/btn_next.gif' alt='이후 10페이지이동' />";
            pageBuilder.AppendLine("<li class='next_li'>" + nextTag.ToString() + "</li>");

            TagBuilder lastTag = new TagBuilder("a");
            lastTag.MergeAttribute("href", pageUrl(totalPages));
            lastTag.InnerHtml = "<img src='/images/btn_omega.gif' alt='끝페이지로' />";
            pageBuilder.AppendLine("<li>" + lastTag.ToString() + "</li>");

            pageBuilder.Append("</ul>");

            return MvcHtmlString.Create(pageBuilder.ToString().Replace("\r\n", string.Empty));
        }

        public static MvcHtmlString PageLink(this HtmlHelper source, int curPage, int totalPages)
        {
            const int pageLimit = 10;

            int startPage = ((curPage - 1) / pageLimit) * pageLimit + 1;
            int endPage = startPage + pageLimit - 1;

            if (totalPages < endPage) endPage = totalPages;

            int nextPage = endPage + 1;

            StringBuilder pageBuilder = new StringBuilder();

            pageBuilder.Append("<ul class='page_view'>");

            TagBuilder first = new TagBuilder("a");
            first.MergeAttribute("href", "javascript:Search(1);");
            first.InnerHtml = "<img src='/images/btn_alpha.gif' alt='첫페이지로' />";
            pageBuilder.AppendLine("<li>" + first.ToString() + "</li>");

            TagBuilder prevTag = new TagBuilder("a");
            prevTag.MergeAttribute("href", string.Format(@"javascript:Search({0});", startPage == 1 ? 1 : startPage - 1));
            prevTag.InnerHtml = "<img src='/images/btn_prev.gif' alt='이전 10페이지이동' />";
            pageBuilder.AppendLine("<li class='prev_li'>" + prevTag.ToString() + "</li>");

            for (int i = startPage; i <= endPage; i++)
            {
                TagBuilder tag = new TagBuilder("a");
                tag.MergeAttribute("href", string.Format(@"javascript:Search({0});", i));
                tag.InnerHtml = "" + i;
                if (i == curPage)
                    tag.Attributes.Add("style", "font-weight:bold;color:red");
                pageBuilder.AppendLine(string.Format(@"<li>{0}{1}</li>", tag.ToString(), (i == pageLimit || i == totalPages) ? string.Empty : " | "));
            }

            TagBuilder nextTag = new TagBuilder("a");
            nextTag.MergeAttribute("href", nextPage < totalPages ? string.Format(@"javascript:Search({0});", nextPage) : string.Format(@"javascript:Search({0});", totalPages));
            nextTag.InnerHtml = "<img src='/images/btn_next.gif' alt='이후 10페이지이동' />";
            pageBuilder.AppendLine("<li class='next_li'>" + nextTag.ToString() + "</li>");

            TagBuilder lastTag = new TagBuilder("a");
            lastTag.MergeAttribute("href", string.Format(@"javascript:Search({0});", totalPages));
            lastTag.InnerHtml = "<img src='/images/btn_omega.gif' alt='끝페이지로' />";
            pageBuilder.AppendLine("<li>" + lastTag.ToString() + "</li>");

            pageBuilder.Append("</ul>");

            return MvcHtmlString.Create(pageBuilder.ToString().Replace("\r\n", string.Empty));
        }

        public static MvcHtmlString DropDownList(this HtmlHelper htmlHelper, string name, IEnumerable<SelectListItem> selectList, string optionLabel, Object htmlAttributes, string[] SelectedValues)
        {
            return DropDownList(htmlHelper, name, selectList, optionLabel, htmlAttributes, SelectedValues, new string[] { });
        }

        public static MvcHtmlString DropDownList(this HtmlHelper htmlHelper, string name, IEnumerable<SelectListItem> selectList, string optionLabel, Object htmlAttributes, string[] SelectedValues, string[] ExceptValues)
        {
            if (SelectedValues != null)
            {
                //Selected 초기화
                if (selectList != null)
                {
                    selectList = selectList.Select(x => new SelectListItem
                    {
                        Text = x.Text,
                        Value = x.Value,
                        Selected = false
                    }).ToList();
                }

                //Selected 설정
                foreach (var item in SelectedValues)
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        selectList = selectList.Select(x => new SelectListItem
                        {
                            Text = x.Text,
                            Value = x.Value,
                            Selected = (string.Compare(x.Value, item, true) == 0) ? true : x.Selected
                        }).ToList();
                    }
                }

            }

            return System.Web.Mvc.Html.SelectExtensions.DropDownList(htmlHelper, name, selectList, optionLabel, htmlAttributes);
        }

        public static string ToStringExt(this Object target, string defaultValue)
        {
            try
            {
                return String.IsNullOrEmpty(target.ToString()) ? defaultValue : target.ToString();
            }
            catch
            {
                return defaultValue;
            }
        }

        public static int ToIntExt(this Object target, int defaultValue)
        {
            int convertValue = 0;

            return int.TryParse(target.ToStringExt(defaultValue.ToString()), out convertValue) ? convertValue : defaultValue;
        }
    }
}