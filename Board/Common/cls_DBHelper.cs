﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Data;

namespace Board.Common
{
    public class cls_DBHelper
    {
        private static bool? _isreal = null;
        public static bool IsReal
        {
            get
            {
                if (_isreal == null)
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["DB_MODE"].ToString().Trim().ToUpper() == "T")
                    {
                        _isreal = true;
                    }
                    else
                    {
                        _isreal = false;
                    }
                }

                if (_isreal == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }

        public static string N_DB_Chunjae
        {
            get
            {
                // Web.config > AppSettings에 접근해서 conStr이라는 변수를 가져온다.
                string strTemp = System.Configuration.ConfigurationManager.AppSettings["conStr"].ToString();
                return strTemp;
            }
        }

        public static int ExecuteCommandByProcedure(string sql)
        {
            using (SqlConnection conn = new SqlConnection(N_DB_Chunjae))
            {
                SqlCommand cmd = new SqlCommand(sql, conn);

                conn.Open();
                int num = cmd.ExecuteNonQuery();
                conn.Close();

                return num;
            }
        }

        public static int ExecuteCommandByProcedure(string sql, params SqlParameter[] values)
        {
            using (SqlConnection conn = new SqlConnection(N_DB_Chunjae))
            {
                SqlCommand cmd = new SqlCommand(sql, conn);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddRange(values);
                conn.Open();
                int num = cmd.ExecuteNonQuery();
                conn.Close();

                return num;
            }
        }

        public static DataTable GetDataTableByProcedure(string sql)
        {
            using (SqlConnection conn = new SqlConnection(N_DB_Chunjae))
            {
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                cmd.CommandType = CommandType.StoredProcedure;
                conn.Open();
                sda.Fill(dt);

                return dt;
            }
        }

        public static DataTable GetDataTableByProcedure(string sql, params SqlParameter[] values)
        {
            using (SqlConnection conn = new SqlConnection(N_DB_Chunjae))
            {
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandTimeout = 6000;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddRange(values);
                conn.Open();
                sda.Fill(dt);
                conn.Close();

                return dt;
            }
        }

        public static DataSet GetDataSetByProcedure(string sql)
        {
            using (SqlConnection conn = new SqlConnection(N_DB_Chunjae))
            {
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();

                cmd.CommandTimeout = 300;

                //cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandType = CommandType.Text;
                //conn.Open();
                sda.Fill(ds);

                return ds;
            }
        }

        public static DataSet GetDataSetByProcedure(string sql, params SqlParameter[] values)
        {
            using (SqlConnection conn = new SqlConnection(N_DB_Chunjae))
            {
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 6000;
                cmd.Parameters.AddRange(values);
                //conn.Open();
                sda.Fill(ds);

                return ds;
            }
        }
    }
}
