﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Board.Common
{
    public class cls_Date
    {
        String str, date, time, year;
        public String DateOrTime(DateTime dt) {

            str = "";
            year = dt.ToString("yy");
            date = dt.ToString("MM-dd");
            time = dt.ToString("hh:mm");

            // 올해에 작성된 경우 년도 제외
            if (DateTime.Now.ToString("yy") != year) { str += year + " "; }
            // 오늘 작성된 경우 시간 표시, 이외는 날짜표시
            if (DateTime.Now.ToString("yy-MM-dd") == year + "-" + date) { str += time; }
            else { str += date; }

            return str;
        }

        public String TimeOrDateTime(DateTime dt)
        {
            str = "";
            year = dt.ToString("yy");
            date = dt.ToString("MM-dd");
            time = dt.ToString("hh:mm");

            // 올해에 작성된 게시글일 경우 년도 제외
            if (DateTime.Now.ToString("yy") != year) { str += year + " "; }
            // 오늘 작성된 게시글일 경우 날짜 제외
            if (DateTime.Now.ToString("yy-MM-dd") == year + "-" + date) { str += time; }
            else { str += $"{date} {time}"; }

            return str;

        }

        public List<DateTime> OneOrBoth(DateTime dt1, DateTime dt2)
        {
            List<DateTime> list = new List<DateTime>();
            // 두 날짜가 같을 경우 하나만 반환
            if (dt1 == dt2) { list.Add(dt1);  }
            else
            {
                list.Add(dt1);
                list.Add(dt2);
            }

            return list;
        }
    }
}