﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Board.Common
{
    public class cls_JsMethod
    {
        public static void JsAlert(List<string> LostGetData)
        {
            if (LostGetData.Count == 1) LostGetData.Add(string.Empty);
            HttpContext.Current.Response.Write(
                string.Format(@"<script>alert('{0}');{1}</script>"
                , LostGetData[0]
                , LostGetData[1])
            );
        }
    }
}