﻿using System;
using System.Linq;
using System.Web.Mvc;
using Board.Models;
using Board.Common;
using System.Data;
using System.Web.Script.Serialization;
using System.Collections.Generic;
using System.Web;

namespace Board.Controllers
{
    public class ArticleController : Controller
    {
        DataTable dt = new DataTable();
        ArticleActs articlActs = new ArticleActs();

        // Controllers/{ClassName}/{FuncName} = Views/{folder}/{viewFile}
        // {folder} = {ClassName}}
        // {viewFile} = {FuncName}

        /// <summary>
        /// GET Articles
        /// <returns> View </returns>     
        /// </summary>
        public ActionResult ArticleList()
        {
            // ID가 없으면 게시판 1 리스트를 반환하도록 설정
            if (Session["ID"] == null) { return RedirectToAction("Index", "Login"); }

            // 타입, 검색 텍스트, 현재 페이지, 페이지 글 수(사0이즈)가 들어있는 request.params 전달
            var query = Request.QueryString;
            // Article 가져오기
            dt = articlActs.GetDataTable(query);

            // DataSet 자체는 ViewBag으로 넘길 수 있지만 enumerable하지 않아서 DataTable로 변환 후 AsEnumerable을 사용해 넘긴다.
            // isDel은 *삭제 여부*를 의미하는 것으로 한다.

            ViewBag.ArticleList = dt.AsEnumerable().ToList();

            return View();
        }

        
        //public string ArticleL(String type)
        //{
        //    dt = articlActs.GetList(type);
        //    JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        //    List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
        //    Dictionary<string, object> childRow;
        //    foreach (DataRow row in dt.Rows)
        //    {
        //        childRow = new Dictionary<string, object>();
        //        foreach (DataColumn col in dt.Columns)
        //        {
        //            if (row[col].GetType().FullName == "System.DateTime") {
        //                string string_date = row[col].ToString();
        //                childRow.Add(col.ColumnName, string_date);
        //            } else
        //            {
        //                childRow.Add(col.ColumnName, row[col]);
        //            }
                    
        //        }
        //        parentRow.Add(childRow);
        //    }
        //    return jsSerializer.Serialize(parentRow);
        //}

        /// <summary>
        /// GET Article
        /// <param name=""></param>
        /// </summary>
        public ActionResult ArticleView(String type, int? idx)
        {
            ActionResult redirectPage = View();
            // index가 없으면 게시판 1 리스트를 반환하도록 설정
            if (idx == null) return RedirectToAction("ArticleList", "Article", new { @type = "1" });
            int Idx = idx ?? 1;
            // Article 가져오기
            dt = articlActs.GetDetail(Idx);

            // DataSet 자체는 ViewBag으로 넘길 수 있지만 enumerable하지 않아서 DataTable로 변환 후 AsEnumerable을 사용해 넘긴다.
            var list = dt.AsEnumerable().Where(a => Convert.ToInt32(a["Idx"]) == idx
            && Convert.ToBoolean(a["isDel"]) == false).Select(a => new Article
            {
                Index = (int)a["Idx"],
                AuthorID = (String)a["AuthorID"],
                AuthorNickName = (String)a["Nickname"],
                Title = (String)a["Title"],
                Contents = (String)a["Contents"],
                ReadCnt = (int)a["ReadCnt"],
                RegDate = (DateTime)a["RegDate"],
                ModDate = (DateTime)a["ModDate"],
                isDel = (Boolean)a["isDel"],
                isTop = (Boolean)a["isTop"],
                Type = Convert.ToInt32(a["Type"])

            }).ToList();
            ViewBag.Article = list;

            try
            {
                // Session에 ID가 있는 경우 Author ID와 비교하여 조회수 증가 (확인 필요)
                if (Session["ID"].ToString() != list[0].AuthorID)
                {
                    string readCnt_sql = $@"UPDATE [dbo].[TBL_BOARD_Article] SET [ReadCnt] = {list[0].ReadCnt + 1} WHERE [dbo].[TBL_BOARD_Article].[Idx] = {list[0].Index}";

                    cls_DBHelper.ExecuteCommandByProcedure(readCnt_sql);
                }
            }
            catch (System.Exception)
            {
                redirectPage = RedirectToAction("Index", "Login");
            }

            // CommentActs 객체 불러오기
            CommentActs commentActs = new CommentActs();

            // CommentAction.Select (프록시저 호출)로 데이터 테이블 받아오기
            int index = idx ?? 0;
            DataTable dt2 = new DataTable();
            dt2 = commentActs.DBSelect(index);

            // List로 필요한 데이터 받기
            var commentList0 = dt2.AsEnumerable().Where(x => Convert.ToInt32(x["ParentIdx"]) == 0).ToList();
            var commentList1 = dt2.AsEnumerable().Where(x => Convert.ToInt32(x["ParentIdx"]) != 0).ToList();

            // ViewBag에 넣기
            if (commentList0 != null) ViewBag.commentList0 = commentList0;
            if (commentList1 != null) ViewBag.commentList1 = commentList1;

            return redirectPage;
        }

        /// <summary>
        /// 댓글 데이터 삭제 (isDel True)
        /// <param name=""></param>
        /// </summary>
        /// 
        [HttpPost]
        public JsonResult ArticleComment_add_fix(FormCollection collection)
        {
            CommentActs commentActs = new CommentActs();
            commentActs.InsertOrUpdate(collection);

             //return RedirectToAction("ArticleView", "Article", new { @type = collection["type"], @idx = collection["idx"] });
            return Json("OK");
        }

        /// <summary>
        /// 댓글 데이터 삭제
        /// <param name=""></param>
        /// </summary>
        /// 
        [HttpPost]
        public JsonResult ArticleComment_del(FormCollection collection)
        {
            CommentActs commentActs = new CommentActs();
            commentActs.Delete(collection);

            //return RedirectToAction("ArticleView", "Article", new { @type = collection["type"], @idx = collection["idx"] });
            return Json("OK");
        }

        /// <summary>
        /// Add - Fix Article
        /// GET 요청 > 추가, 수정 Form 던져주기
        /// </summary>
        /// <returns> View </returns>  

        public ActionResult ArticleForm(String type, int? idx)
        {
            if (Session["ID"] == null) { return RedirectToAction("Index", "Login"); }
            // idx가 있으면 이전 내용을 불러와서 넣어주기 (수정)
            if (idx != null)
            {
                int Idx = idx ?? 0;
                // Article 가져오기
                dt = articlActs.GetDetail(Idx);

                var list = dt.AsEnumerable().Where(a => Convert.ToInt32(a["Idx"]) == idx).Select(a => new Article
                {
                    Index = (int)a["Idx"],
                    AuthorID = (String)a["AuthorID"],
                    Title = (String)a["Title"],
                    Contents = (String)a["Contents"],
                    Type = Convert.ToInt32(a["Type"]),
                    ReadCnt = Convert.ToInt32(a["ReadCnt"]),
                    RegDate = Convert.ToDateTime(a["RegDate"]),
                    isTop = (Boolean)a["isTop"],
                    isDel = (Boolean)a["isDel"],

                }).ToList();

                ViewBag.index = idx;
                ViewBag.Atitle = list[0].Title;
                ViewBag.authorID = list[0].AuthorID;
                ViewBag.contents = list[0].Contents;
                ViewBag.type = list[0].Type;
                ViewBag.isTop = list[0].isTop;
                ViewBag.isDel = list[0].isDel;
                ViewBag.ReadCnt = list[0].ReadCnt;
                ViewBag.regDate = list[0].RegDate;
            }
            else
            {
                ViewBag.Atitle = "";
                ViewBag.authorID = 
                ViewBag.contents = "";
                ViewBag.type = Convert.ToInt32(type);
                ViewBag.isTop = 0;
                ViewBag.isDel = 0;
                ViewBag.ReadCnt = null;
                ViewBag.regDate = null;
            }

            return View();
        }

        /// <summary>
        /// Add - Fix Article
        /// 
        /// </summary>
        /// <returns> View </returns>  
        [HttpPost]
        public ActionResult ArticleForm(FormCollection collection)
        {
            if (collection.Get("AuthorID").Count() < 1) collection["AuthorID"] = (string)Session["ID"];

            // DB 쪽에 Insert 실행하기.
            articlActs.Set(collection);
            
            // 실행 후 게시판 반환
            return RedirectToAction("ArticleView", "Article", new { @type = collection["Type"], @idx = collection["Index"] });
        }

        [HttpGet]
        public ActionResult ArticleDelete(String type, int idx)
        {
            articlActs.Delete(idx);
            return RedirectToAction("ArticleList", "Article", new { @type = type });
        }

    }
}
