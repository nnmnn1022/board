﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using Board.Common;
using Board.Models;

namespace Board.Controllers
{
    public class LoginController : Controller
    {
        public String id;

        # region 생성자
        public LoginController()
        {
        
        }
        # endregion

        # region 로그인 GET
        public ActionResult Index()
        {
            // http를 https로 변경
            if (Request.Url.ToString().IndexOf("localhost") == -1 && Request.Url.ToString().Contains("http://"))
            {
                Response.Redirect(Request.Url.ToString().Replace("http://", "https://"));
            }
            Debug.WriteLine("Here");
            return View();
        }
        # endregion

        # region 로그인 POST
        [HttpPost]
        public ActionResult Index(FormCollection collection)
        {
            // 리다이렉트 할 페이지 설정.
            ActionResult redirctPage = View();
            String id = collection["txt_UserName"].ToString();
            String pw = collection["pas_PassWord"].ToString();

            //ActionResult redirectPage;
            MemberActs memberActs = new MemberActs();
            var getMember = memberActs.GetMember(id).AsEnumerable();

            if (!getMember.Any())
            {
                cls_JsMethod.JsAlert(new List<string>() { "아이디 또는 패스워드가 일치하지 않습니다!",
                "history.back(-1);" });

            } else if (getMember.All(a => a["Password"].ToString() != pw || a["ID"].ToString() != id)) {
                
                cls_JsMethod.JsAlert(new List<string>() { "아이디 또는 패스워드가 일치하지 않습니다!",
                "history.back(-1);" }); // history.back(-1) 브라우저가 세션 기록 바로 뒤 페이지로 이동하도록 함.

                // redircet 자체를 실행시키지 않는 방법 확인필요
            }
            else
            {
                var member = getMember.Where(a => a["Password"].ToString() == pw && a["ID"].ToString() == id).Select(a => new Member
                {
                    ID = (string)a["ID"],
                    NickName = (string)a["NickName"]
                });

                Session["ID"] = member.First().ID;
                redirctPage = RedirectToAction("ArticleList", "Article", new { @type = "1", @pageNo = "1", @pageSize = "10"});
            }

            return redirctPage;
          
        }
        # endregion
    }
}
