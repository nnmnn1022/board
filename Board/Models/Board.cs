﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Board.Models
{
    public class Board
    {
        public int ID { get; set; }
        public String Title { get; set; }
        public List<Article> Articles { get; set; }
    }
}