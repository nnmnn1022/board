﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Board.Models
{
    public class Comment
    {
        public int Idx { get; set; }
        public string AuthorID { get; set; }
        public int ArticleIdx { get; set; }
        public int ParentIdx { get; set; }
        public string Contents { get; set; }
        public DateTime RegDate { get; set; }
        public DateTime ModDate { get; set; }
        public bool isDel { get; set; }
    }
}