﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Board.Common;
using Board.tmp;

namespace Board.Models
{
    public class ArticleActs
    {
        DataTable dt = new DataTable();

        public DataTable GetDataTable(NameValueCollection info)
        {
            String sql = "USP_BOARD_Article_List";

            string type = info["type"] ?? "1";
            string searchText = info["searchText"] ?? "";
            string pageNo = info["pageNo"] ?? "1";
            string pageSize = info["pageSize"] ?? "10";
            

            // 파라미터 입력
            SqlParameter[] param =
            {
                new SqlParameter("@in_Type", type),
                new SqlParameter("@in_SearchText", searchText),
                new SqlParameter("@in_PageNo", pageNo),
                new SqlParameter("@in_PageSize", pageSize)

            };

            dt = cls_DBHelper.GetDataTableByProcedure(sql, param);

            return dt;
        }

        public DataTable GetDetail(int idx)
        {
            String sql = "USP_BOARD_Article_View";
            SqlParameter[] param =
            {
                new SqlParameter("@in_Idx", idx)
            };
            dt = cls_DBHelper.GetDataTableByProcedure(sql, param);

            return dt;
        }

        public int Delete(int idx)
        {
            string sql = $@"UPDATE [dbo].[TBL_BOARD_Article] 
                         SET [isDel] = 1
                         WHERE [dbo].[TBL_BOARD_Article].[Idx] = { idx }";
            int num = cls_DBHelper.ExecuteCommandByProcedure(sql);

            return num;
        }

        public void Set(FormCollection collection)
        {
            int ReadCnt = 0;
            string regDate = (DateTime.Now).ToString("yyyy-MM-dd HH:mm");
            string sql = "";

            if (!string.IsNullOrEmpty(collection.Get("Index")))
            {
                // 수정
                sql = $@"UPDATE [dbo].[TBL_BOARD_Article] 
                         SET [Type] = {Convert.ToInt32(collection["Type"])}
                          ,[Title] = '{collection["Title"]}'
                          ,[Contents] = '{collection["Contents"]}'
                          ,[ModDate] = '{regDate}'
                          ,[isDel] = {Convert.ToInt32(Convert.ToBoolean(collection["isDel"]))}
                          ,[isTop] = {Convert.ToInt32(Convert.ToBoolean(collection["isTop"]))}
                         WHERE [dbo].[TBL_BOARD_Article].[Idx] = { Convert.ToInt32(collection["Index"]) }";

            }
            else
            {
                // 추가
                sql = $@"INSERT INTO [dbo].[TBL_BOARD_Article] ([Type],[AuthorID],[Title],[Contents],[ReadCnt], [RegDate], [ModDate],[isDel],[isTop]) VALUES ({Convert.ToInt32(collection["Type"])}, '{collection["AuthorID"]}', '{collection["Title"]}', '{collection["Contents"]}',{ReadCnt}, '{regDate}', '{regDate}', {Convert.ToInt32(Convert.ToBoolean(collection["isDel"]))}, {Convert.ToInt32(Convert.ToBoolean(collection["isTop"]))} )";

            }

            cls_DBHelper.ExecuteCommandByProcedure(sql);


        }
    }
}