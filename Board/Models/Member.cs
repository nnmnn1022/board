﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Board.Models
{
    public class Member
    {
        /// <summary>
        /// User ID
        /// </summary>
        public String ID { get; set; }

        /// <summary>
        /// 닉네임
        /// </summary>
        public String NickName { get; set; }

        /// <summary>
        /// 비밀번호
        /// </summary>
        public String PW { get; set; }

        /// <summary>
        /// 그룹 번호
        /// </summary>
        public int Group { get; set; }

        /// <summary>
        /// 가입일
        /// </summary>
        public DateTime RegDate { get; set; }

        /// <summary>
        /// 어드민 여부
        /// </summary>
        public bool isAdmin { get; set; }

        /// <summary>
        /// 밴 여부
        /// </summary>
        public bool isBan { get; set; }

    }
}