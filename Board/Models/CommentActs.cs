﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Board.Common;

namespace Board.Models
{
    public class CommentActs
    {
        // Select
        public DataTable DBSelect(int idx)
        {
            DataTable dt = new DataTable();
            // 프록시저 이름 입력
            string sql = "USP_BOARD_Comment_List";

            // 파라미터 입력
            SqlParameter[] param =
            {
                new SqlParameter("@articleIdx", idx)
            };

            // DB 실행
            dt = cls_DBHelper.GetDataTableByProcedure(sql, param);

            return dt;
        }


        // Insert
        public int InsertOrUpdate(FormCollection collection)
        {
            int num = 0;

            // 프록시저 이름
            string sql = "USP_BOARD_Comment_Insert_Update";

            // 댓글 idx가 들어오지 않은 경우 (새로운 댓글 작성)
            int idx = collection["idx"] == "" ? 0 : Convert.ToInt32(collection["idx"]);

            SqlParameter[] param =
            {
                new SqlParameter("@idx", idx),
                new SqlParameter("@authorID", collection["authorID"]),
                new SqlParameter("@articleIdx", collection["articleIdx"]),
                new SqlParameter("@parentIdx", collection["parentIdx"]),
                new SqlParameter("@contents", collection["contents"]),
                new SqlParameter("@isDel", false)
            };

            // DB 연결
            num = cls_DBHelper.ExecuteCommandByProcedure(sql, param);

            return num;
        }

        public int Delete(FormCollection collection)
        {
            int num = 0;

            // 프록시저 이름
            string sql = "USP_BOARD_Comment_Delete";

            SqlParameter[] param =
            {
                new SqlParameter("@commentIdx", collection["commentIdx"]),
            };

            // DB 연결
            num = cls_DBHelper.ExecuteCommandByProcedure(sql, param);

            return num;
        }

    }
}