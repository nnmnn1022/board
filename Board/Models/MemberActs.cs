﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Board.tmp;
using System.Data;
using Board.Common;

namespace Board.Models
{
    public class MemberActs
    {
        DataSet ds = new DataSet();

        
        public DataTable GetMember(String paramMemberID)
        {
            String sql = $@"SELECT *

                        FROM [dbo].[TBL_BOARD_User]

                        WHERE [dbo].[TBL_BOARD_User].[ID] = '{paramMemberID}'";

            // Article 가져오기
            ds = cls_DBHelper.GetDataSetByProcedure(sql);
            DataTable dt = ds.Tables[0];
            // LinQ
            //dt.AsEnumerable().Where(x => x["ID"].ToString() == paramMemberID).SingleOrDefault()
            //var list = dt.AsEnumerable().Where(x => x["ID"].ToString() == paramMemberID);
            
            return dt;
        }

    }
}