﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Board.Models
{
    public class Article
    {
        /// <summary>
        /// 게시글 ID
        /// </summary>
        public int Index { get; set; }

        /// <summary>
        /// 작성자
        /// </summary>

        public String AuthorID { get; set; }

        /// <summary>
        /// 작성자
        /// </summary>

        [Required]
        [Display(Name ="닉네임")]
        public String AuthorNickName { get; set; }

        /// <summary>
        /// 제목
        /// </summary>
        [Required]
        [Display(Name = "제목")]
        public String Title { get; set; }

        /// <summary>
        /// 내용
        /// </summary>
        ///
        [Required]
        [Display(Name = "내용")]
        public String Contents { get; set; }

        /// <summary>
        /// 댓글
        /// </summary>
        public int ReadCnt { get; set; }

        /// <summary>
        /// 작성 일시
        /// </summary>
        public DateTime RegDate { get; set; }

        /// <summary>
        /// 수정 일시
        /// </summary>
        public DateTime ModDate { get; set; }

        /// <summary>
        /// 노출 여부
        /// </summary>
        /// 
        [Display(Name = "삭제")]
        public bool isDel { get; set; }

        /// <summary>
        /// <summary>
        /// 상단 노출 여부
        /// </summary>
        /// 
        [Display(Name = "공지")]
        public bool isTop { get; set; }

        /// <summary>
        /// 게시판 타입
        /// </summary>
        /// 
        [Required]
        [Display(Name = "게시판")]
        public int Type { get; set; }
    }
}